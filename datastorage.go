package main

//	Importing needed packages
import "time"

//								Structs
//-----------------------------------------------------------------------------

//  Struct to store data about each country
type country struct {
	Name    string `json:"name"`
	Capital string `json:"capital"`
	Flag    string `json:"flag"`
}

//  Struct to store data about each species
type species struct {
	Key            int    `json:"key"`
	Kingdom        string `json:"kingdom"`
	Phylum         string `json:"phylum"`
	Order          string `json:"order"`
	Family         string `json:"family"`
	Genus          string `json:"genus"`
	ScientificName string `json:"scientificName"`
	CanonicalName  string `json:"canonicalName"`
	Year           string `json:"year"`
}

//	Struct to store JSON array with the occurences from GBIF
type occurrences struct {
	Results []result `json:"results"`
}

//	Struct with data about a single occurence from GBIF
type result struct {
	SpeciesKey       int    `json:"speciesKey"`
	AcceptedTaxonKey int    `json:"acceptedTaxonKey"`
	ScientificName   string `json:"scientificName"`
}

//	A struct with data about the country and all the species in that country plus their species key
type countryWithSpecies struct {
	Name       string   `json:"name"`
	Capital    string   `json:"capital"`
	Flag       string   `json:"flag"`
	Species    []string `json:"species"`
	SpeciesKey []int    `json:"speciesKey"`
}

//  Struct for diagnostics data
type diagnostics struct {
	Gbif          string `json:"gbif"`
	Restcountries string `json:"restcountries"`
	Version       string `json:"version"`
	Uptime        string `json:"uptime"`
}

//							Global variables and constants
//-----------------------------------------------------------------------------

var uptime time.Time                                                             // Stores the time at start of application
const apiVersion string = "v1"                                                   //	Current version of api
const gbifSpecies string = "http://api.gbif.org/v1/species/"                     //	Path in gbif api to get species
const gbifOccurence string = "http://api.gbif.org/v1/occurrence/search?country=" //	Path in api to occurences in country
const restcountriesPath string = "http://restcountries.eu/rest/v2/alpha/"        //	Path in api to country data in restcountries

//	Instructions on how to use api if not used correctly
const badRequestUsage string = "Usage:\n\tCountry:\t/country/countrycode\n\tSpecies:\t/species/specieskey\n\tDiagnostics:\t/diag"
