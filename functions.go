package main

//	Importing needed packages
import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
	"time"
)

//							Global functions
//-----------------------------------------------------------------------------

//	Function that checks for errors
func errCheck(e error) {
	if e != nil {
		log.Print(e)
		return
	}
}

//							Handler functions
//-----------------------------------------------------------------------------

//  Species endpoint
func handlerSpecies(w http.ResponseWriter, r *http.Request) {

	//	Declare struct objects for later use
	var speciesObj species
	var speciesObjYear species

	//	Splits url into different strings
	//	and sends bad status request if species key is missing
	parts := strings.Split(r.URL.Path, "/")

	name := parts[2] //  Gets name of requested species

	res, e := http.Get(gbifSpecies + name) //  Gets data from api about species with chosen name

	errCheck(e)

	e = json.NewDecoder(res.Body).Decode(&speciesObj) //	Decoding data from api

	errCheck(e)

	res, e = http.Get(gbifSpecies + name + "/name") //  Gets year data from API

	errCheck(e)

	res.Body.Close() //	Closes res to avoid leak

	e = json.NewDecoder(res.Body).Decode(&speciesObjYear) //	decodes the year data

	errCheck(e)

	speciesObj.Year = speciesObjYear.Year //	Updates the year of the main struct with the one from the second get

	temp := new(bytes.Buffer) //	Creates a temporary binary to store and decode json

	encoder := json.NewEncoder(temp) // Encodes into temp
	encoder.Encode(speciesObj)       // Encodes the message

	w.Header().Add("Content-Type", "application/json") //	Displays the message formatted in json
	w.WriteHeader(http.StatusOK)

	io.Copy(w, temp) //	Prints to server
}

//  Country endpoint
func handlerOccurrence(w http.ResponseWriter, r *http.Request) {

	// Declare struct objects and variable for later use
	var urladdress string
	var fetched occurrences
	var displayed occurrences
	var displayed2 country
	var displayed3 countryWithSpecies

	limit := r.FormValue("limit") // Gets the value of limit from request

	parts := strings.Split(r.URL.Path, "/") // Splits url into parts divided by '/'

	code := parts[2] //	Gets country code from the split parts

	if limit != "" { //	Prepares the address, if limit has a value use the adress with the value else limit will not be used
		urladdress = gbifOccurence + code + "&limit=" + limit
	} else {
		urladdress = gbifOccurence + code
	}

	res, e := http.Get(urladdress) //	Gets info from the address in the string

	errCheck(e)

	e = json.NewDecoder(res.Body).Decode(&fetched) // Decodes the data

	errCheck(e)

	var occurenceInArray bool

	// Loops through the fetched data and removes duplicates
	for i := 0; i < len(fetched.Results); i++ {

		occurenceInArray = false

		for j := 0; j < len(displayed.Results); j++ {

			if fetched.Results[i] == displayed.Results[j] {
				occurenceInArray = true
			}
		}

		if !occurenceInArray {
			displayed.Results = append(displayed.Results, fetched.Results[i])
		}
	}

	res, e = http.Get(restcountriesPath + code) // Sends get request about 'code' country

	errCheck(e)

	e = json.NewDecoder(res.Body).Decode(&displayed2)

	res.Body.Close() //	Closes res to avoid leak

	errCheck(e)

	// Gives data to displayed3
	displayed3.Name = displayed2.Name
	displayed3.Capital = displayed2.Capital
	displayed3.Flag = displayed2.Flag

	//	Splits information from GBIF into two arrays
	for i := 0; i < len(displayed.Results); i++ {

		displayed3.Species = append(displayed3.Species, displayed.Results[i].ScientificName)

		//If species has no speciesKey, the acceptedTaxonKey is used instead
		if displayed.Results[i].SpeciesKey != 0 {
			displayed3.SpeciesKey = append(displayed3.SpeciesKey, displayed.Results[i].SpeciesKey)
		} else {
			displayed3.SpeciesKey = append(displayed3.SpeciesKey, displayed.Results[i].AcceptedTaxonKey)
		}
	}

	temp := new(bytes.Buffer) // Creates a binary storage to encode json

	encoder := json.NewEncoder(temp) // Encodes into temp
	encoder.Encode(displayed3)       // Encodes displayed3

	w.Header().Add("Content-Type", "application/json") // Displays the message formatted in json

	w.WriteHeader(http.StatusOK)

	io.Copy(w, temp) // Prints to server
}

// Diagnostics endpoint
func handlerDiagnostics(w http.ResponseWriter, r *http.Request) {

	var diag diagnostics //  Declares a diag struct element

	//  A Get request used to check the API status
	res, e := http.Get(gbifSpecies + "5787161")

	errCheck(e)

	diag.Gbif = res.Status //  Sets status code for GBIF API

	//  A Get request, only used to check the API status
	res, e = http.Get(restcountriesPath + "IR")

	errCheck(e)

	diag.Restcountries = res.Status //  Sets status code for RESTcountries API

	res.Body.Close() //	Closes res to avoid leak

	diag.Version = apiVersion                 //	Sets version
	diag.Uptime = time.Since(uptime).String() //	Sets uptime

	temp := new(bytes.Buffer) //  Declares a byte array used as buffer

	encoder := json.NewEncoder(temp) //	Create an encoder with temp as destination
	encoder.Encode(diag)             //	Encodes diag

	w.Header().Add("Content-Type", "application/json") //	Displays the message formatted in json
	w.WriteHeader(http.StatusOK)

	io.Copy(w, temp) // Prints to server
}

//  Handles mistyped url's and sends an error
func handlerBadRequest(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "Bad Request", http.StatusBadRequest)
	fmt.Fprintf(w, badRequestUsage)
}
