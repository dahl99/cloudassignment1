package main

//  Importing needed packages
import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

//								Main
//-----------------------------------------------------------------------------

func main() {

	//  Keeps track of time at start
	uptime = time.Now()

	//	Gets port and checks if it's "8080", if not port is set to be "8080"
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
		fmt.Println("Port not found! Setting to 8080")
	}

	//  HandleFunc() for each endpoint
	http.HandleFunc("/", handlerBadRequest)
	http.HandleFunc("/species/", handlerSpecies)
	http.HandleFunc("/country/", handlerOccurrence)
	http.HandleFunc("/diag/", handlerDiagnostics)

	//  Prints which port server is listening on
	fmt.Println("Listening on port " + port)

	//  Initialises server
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
